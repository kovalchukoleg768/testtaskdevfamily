<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Http\Resources\Employee\EmployeeResource;
use App\Models\Employee;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class EmployeeController extends Controller
{
    public function index(Request $request): AnonymousResourceCollection
    {
        $employees = Employee::query()->paginate();

        return EmployeeResource::collection($employees);
    }

    public function create(EmployeeRequest $request): EmployeeResource
    {
        /** @var Employee $employee */
        $employee = Employee::query()
            ->create([
                'name' => $request->get('name'),
                'surname' => $request->get('surname'),
                'patronymic_name' => $request->get('patronymic_name'),
                'salary' => $request->get('salary'),
                'gender' => $request->get('gender'),
            ]);

        return new EmployeeResource($employee);
    }

    public function update(EmployeeRequest $request, $id): EmployeeResource|JsonResponse
    {
        /** @var Employee $employee */
        $employee = Employee::query()->find($id);

        if (!$employee) {
            return response()->json(['error' => 'Неверный id сотрудника!'], 404);
        }

        $employee->update([
            'name' => $request->get('name'),
            'surname' => $request->get('surname'),
            'patronymic_name' => $request->get('patronymic_name'),
            'salary' => $request->get('salary'),
            'gender' => $request->get('gender'),
        ]);

        return new EmployeeResource($employee);
    }

    public function delete(Request $request, $id): Response|JsonResponse
    {
        /** @var Employee $employee */
        $employee = Employee::query()->find($id);

        if (!$employee) {
            return response()->json(['error' => 'Неверный id сотрудника!'], 404);
        }

        $employee->departments()->detach();
        $employee->delete();

        return response()->noContent();
    }
}
