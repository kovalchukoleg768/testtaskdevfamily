<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::get('/', [Controller::class, 'getAllApiMethods']);

Route::group(['prefix' => 'department'], function () {
    Route::get('/', [DepartmentController::class, 'index']);
    Route::put('/create', [DepartmentController::class, 'create']);
    Route::put('/{id}/update', [DepartmentController::class, 'update']);
    Route::delete('/{id}/delete', [DepartmentController::class, 'delete']);
    Route::post('/{department_id}/employee/{employee_id}/add', [DepartmentController::class, 'addEmployee']);
});

Route::group(['prefix' => 'employee'], function () {
    Route::get('/', [EmployeeController::class, 'index']);
    Route::put('/create', [EmployeeController::class, 'create']);
    Route::put('/{id}/update', [EmployeeController::class, 'update']);
    Route::delete('/{id}/delete', [EmployeeController::class, 'delete']);
});
