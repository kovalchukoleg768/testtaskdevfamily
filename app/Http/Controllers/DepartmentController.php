<?php

namespace App\Http\Controllers;

use App\Http\Requests\DepartmentRequest;
use App\Http\Resources\Department\DepartmentAddEmployeeResource;
use App\Http\Resources\Department\DepartmentIndexResource;
use App\Http\Resources\Department\DepartmentResource;
use App\Models\Department;
use App\Models\Employee;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DepartmentController extends Controller
{
    public function index(Request $request)
    {
        $departments = Department::query()
            ->withCount('employees')
            ->with('employees')
            ->paginate();

        $departments->map(function (Department $department) {
           $department->max_salary = $department->employees->max('salary');
        });

        return DepartmentIndexResource::collection($departments);
    }

    public function create(DepartmentRequest $request): DepartmentResource|JsonResponse
    {
        /** @var Department $department */
        $department = Department::query()
            ->create(['title' => $request->get('title')]);

        return new DepartmentResource($department);
    }

    public function update(DepartmentRequest $request, $id): DepartmentResource|JsonResponse
    {
        /** @var Department $department */
        $department = Department::query()->find($id);

        if (!$department) {
            return response()->json(['error' => 'Неверный id отдела!'], 404);
        }

        $department->update(['title' => $request->get('title')]);

        return new DepartmentResource($department);
    }

    public function delete(Request $request, $id): Response|JsonResponse
    {
        /** @var Department $department */
        $department = Department::query()->withCount('employees')->find($id);

        if (!$department) {
            return response()->json(['error' => 'Неверный id отдела!'], 404);
        } elseif ($department->employees_count > 0) {
            return response()->json(['error' => 'В отделе присутствуют сотрудники. Удаление невозможно'], 400);
        }

        $department->delete();

        return response()->noContent();
    }

    public function addEmployee(Request $request, $departmentId, $employeeId): DepartmentAddEmployeeResource|JsonResponse
    {
        /** @var Department $department */
        $department = Department::query()->find($departmentId);

        if (!$department) {
            return response()->json(['error' => 'Неверный id отдела!'], 404);
        }

        /** @var Employee $employee */
        $employee = Employee::query()->find($employeeId);

        if (!$employee) {
            return response()->json(['error' => 'Неверный id сотрудника!'], 404);
        }

        $department->employees()->sync($employeeId);
        $department->load('employees');

        return new DepartmentAddEmployeeResource($department);
    }
}
