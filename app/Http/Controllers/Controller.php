<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function getAllApiMethods(): array
    {
        return [
            '/department' => [
                '/' => [
                    'Тип' => 'Get',
                    'Описание' => 'Получение списка отделов',
                ],
                '/create' => [
                    'Тип' => 'Put',
                    'Описание' => 'Создание отдела',
                    'Входные параметры' => [
                        'title' => 'required,string'
                    ]
                ],
                '/{id}/update' => [
                    'Тип' => 'Put',
                    'Описание' => 'Обновлени отдела',
                    'Входные параметры' => [
                        'title' => 'required,string'
                    ]
                ],
                '/{id}/delete' => [
                    'Тип' => 'Delete',
                    'Описание' => 'Удаление отдела',
                ],
                '/{department_id}/employee/{employee_id}/add' => [
                    'Тип' => 'Post',
                    'Описание' => 'Добавление сотрудника в отдел',
                ],
            ],
            '/employee' => [
                '/' => [
                    'Тип' => 'Get',
                    'Описание' => 'Получение списка сотрудников',
                ],
                '/create' => [
                    'Тип' => 'Put',
                    'Описание' => 'Создание сотрудникв',
                    'Входные параметры' => [
                        'name' => 'required,string',
                        'surname' => 'required,string',
                        'patronymic_name' => 'required,string',
                        'gender' => 'required,(man or woman)',
                        'salary' => 'required,int',
                    ]
                ],
                '/{id}/update' => [
                    'Тип' => 'Put',
                    'Описание' => 'Обновление сотрудникв',
                    'Входные параметры' => [
                        'name' => 'required,string',
                        'surname' => 'required,string',
                        'patronymic_name' => 'required,string',
                        'gender' => 'required,(man or woman)',
                        'salary' => 'required,int',
                    ]
                ],
                '/{id}/delete' => [
                    'Тип' => 'Delete',
                    'Описание' => 'Удаление сотрудника',
                ],
            ],
        ];
    }
}
