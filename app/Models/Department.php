<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;

/**
 * Class Employee
 * @package App\Models\
 *
 * @property integer $id
 * @property string $title
 *
 * @property Collection $employees
 */

class Department extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'departments';

    public function employees(): BelongsToMany
    {
        return $this->belongsToMany(
            Employee::class,
            'employee_department',
            'department_id',
            'employee_id'
        );
    }
}
