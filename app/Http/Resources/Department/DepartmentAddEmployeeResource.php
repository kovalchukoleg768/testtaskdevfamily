<?php

namespace App\Http\Resources\Department;

use App\Models\Department;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Department
 */
class DepartmentAddEmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'employees' => $this->employees->map(function (Employee $employee) {
                return [
                    'id' => $employee->id,
                    'fio' => $employee->getFIO(),
                ];
            }),
        ];
    }
}
