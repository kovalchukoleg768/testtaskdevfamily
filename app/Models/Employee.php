<?php

namespace App\Models;

use App\Enums\Gender;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;

/**
 * Class Employee
 * @package App\Models\
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $patronymic_name
 * @property Gender $gender
 * @property integer $salary
 *
 * @property Collection $departments
 */

class Employee extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'employees';

    protected $casts = [
      'gender' => Gender::class,
    ];

    public function departments(): BelongsToMany
    {
        return $this->belongsToMany(
            Department::class,
            'employee_department',
            'employee_id',
            'department_id'
        );
    }

    public function getFIO(): string
    {
        return trim($this->surname . ' ' . $this->name . ' ' . $this->patronymic_name);
    }

}
