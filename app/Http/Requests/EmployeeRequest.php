<?php

namespace App\Http\Requests;

use App\Enums\Gender;
use Illuminate\Validation\Rule;

class EmployeeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'surname' => ['required', 'string'],
            'patronymic_name' => ['required', 'string'],
            'salary' => ['required', 'integer'],
            'gender' => ['required',  Rule::in([Gender::WOMEN->value, Gender::MAN->value])],
        ];
    }
}
