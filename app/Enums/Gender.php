<?php

namespace App\Enums;

enum Gender: string
{
    case WOMEN = 'women';
    case MAN = 'man';

    public function label(): string
    {
        return match ($this) {
            Gender::WOMEN => 'Женщина',
            Gender::MAN => 'Мужчина',
        };
    }
}
